package testout;

import javax.swing.JPanel;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class Gui extends JFrame {
    public Gui(Boolean frst) {
        if (frst == true) {
            Frame(v.fwidth, v.fheight);
        } else {
            Panel(v.fwidth, v.fwidth);
        }
    }

    private void Panel(int x, int y) {

        JPanel testPanel = new JPanel();
        testPanel.setPreferredSize(new Dimension(x, y));

        createMenu();

        add(testPanel);
        pack();

        setTitle("PanzerHQ");
        setLocation(10, 10);
        setResizable(false);

        setVisible(true);
    }

    private void Frame(int x, int y) {
        v.jf1 = new JFrame();
        v.jf1.setSize(x, y);
        v.jf1.setLocationRelativeTo(null);
        v.jf1.setResizable(false);
        v.jf1.setTitle("Ping Pong");
        v.jf1.requestFocus();
        v.jf1.setVisible(true);
    }

    private void createMenu() {

        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        JMenu fileMenu = new JMenu("File");
        JMenu gameMenu = new JMenu("Game");
        JMenu prefMenu = new JMenu("Preferences");

        menuBar.add(fileMenu);
        menuBar.add(gameMenu);
        menuBar.add(prefMenu);

        Interact.addFileMenuItems(fileMenu);
    }
}
